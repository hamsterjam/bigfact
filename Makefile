TARGET = bigfact

# Programs
CXX = g++
AS  = as

# Local Floders
CPP_SOURCE_DIR = source
HEADER_DIR     = include
ASM_SOURCE_DIR = assembly
OBJECT_DIR     = object
COVER_DIR      = coverage
ASM_OBJECT_DIR = $(OBJECT_DIR)/asssembly

# Find all cpp sources
CPP_PATHS = $(wildcard $(CPP_SOURCE_DIR)/*.cpp)
CPP_FILES = $(CPP_PATHS:$(CPP_SOURCE_DIR)/%=%)

CPP_OBJECTS = $(filter-out main.o,$(CPP_FILES:.cpp=.o))

# Find all assembly sources
ASM_PATHS = $(wildcard $(ASM_SOURCE_DIR)/*.s)
ASM_FILES = $(ASM_PATHS:$(ASM_SOURCE_DIR)/%=%)

ASM_OBJECTS = $(ASM_FILES:.s=.o)

# Flags
CFLAGS = -std=c++11 -Wall
LFLAGS = -I$(HEADER_DIR)

# Debug Variables
DEBUG_TARGET = $(TARGET)_debug
DEBUG_CFLAGS = $(CFLAGS) -g -fprofile-arcs -ftest-coverage
DEBUG_LFLAGS = $(LFLAGS) -lgcov --coverage
DEBUG_OBJECT_DIR = $(OBJECT_DIR)/debug
DEBUG_OBJECTS  = $(addprefix $(DEBUG_OBJECT_DIR)/, $(CPP_OBJECTS))
DEBUG_OBJECTS += $(addprefix $(ASM_OBJECT_DIR)/, $(ASM_OBJECTS))

# Release Variables
RELEASE_TARGET = $(TARGET)
RELEASE_CFLAGS = $(CFLAGS) -O3
RELEASE_LFLAGS = $(LFLAGS)
RELEASE_OBJECT_DIR = $(OBJECT_DIR)/release
RELEASE_OBJECTS  = $(addprefix $(RELEASE_OBJECT_DIR)/, $(CPP_OBJECTS))
RELEASE_OBJECTS += $(addprefix $(ASM_OBJECT_DIR)/, $(ASM_OBJECTS))

.PHONY: default
default: debug

.PHONY: clean
clean:
	@rm -rf $(OBJECT_DIR)
	@rm -rf $(COVER_DIR)
	@rm -f *.gcno
	@rm -f *.gcda
	@rm -f *.gcov

.PHONY: pre_pre
pre_pre:
	@[ -d $(OBJECT_DIR) ]     || mkdir $(OBJECT_DIR)
	@[ -d $(ASM_OBJECT_DIR) ] || mkdir $(ASM_OBJECT_DIR)

.PHONY: pre_debug
pre_debug: pre_pre
	@[ -d $(DEBUG_OBJECT_DIR) ] || mkdir $(DEBUG_OBJECT_DIR)

.PHONY: pre_release
pre_release: pre_pre
	@[ -d $(RELEASE_OBJECT_DIR) ] || mkdir $(RELEASE_OBJECT_DIR)

debug: pre_debug $(DEBUG_OBJECTS) $(CPP_SOURCE_DIR)/main.cpp
	@echo 'Compiling debug build'
	@$(CXX) $(DEBUG_CLFLAGS) $(DEBUG_OBJECTS) $(CPP_SOURCE_DIR)/main.cpp -o $(DEBUG_TARGET) $(DEBUG_LFLAGS)

release: pre_release $(RELEASE_OBJECTS) $(CPP_SOURCE_DIR)/main.cpp
	@echo 'Compiling release build'
	@$(CXX) $(RELEASE_CFLAGS) $(RELEASE_OBJECTS) $(CPP_SOURCE_DIR)/main.cpp -o $(RELEASE_TARGET) $(RELEASE_LFLAGS)

%-test: test/%.cpp pre_debug $(DEBUG_OBJECTS)
	@echo 'Compiling test build...'
	@$(CXX) $(DEBUG_CFLAGS) $(DEBUG_OBJECTS) $< -o $@ $(DEBUG_LFLAGS)

.PHONY: coverage
coverage: $(DEBUG_OBJECTS)
	@rm -rf $(COVER_DIR)
	@mkdir $(COVER_DIR)
	@for f in "$(filter-out $(CPP_SOURCE_DIR)/main.cpp,$(CPP_PATHS))"; do \
		gcov -o $(DEBUG_OBJECT_DIR) -r $$f; \
	done
	@mv *.gcov $(COVER_DIR)

$(DEBUG_OBJECT_DIR)/%.o : $(CPP_SOURCE_DIR)/%.cpp
	@echo 'Compiling '$@'...'
	@$(CXX) $(DEBUG_CFLAGS) -c $< -o $@ $(DEBUG_LFLAGS)

$(RELEASE_OBJECT_DIR)/%.o : $(CPP_SOURCE_DIR)/%.cpp
	@echo 'Compiling '$@'...'
	@$(CXX) $(RELEASE_CFLAGS) -c $< -o $@ $(RELEASE_LFLAGS)

$(ASM_OBJECT_DIR)/%.o : $(ASM_SOURCE_DIR)/%.s
	@echo 'Assembling '$@'...'
	@$(AS) $< -o $@
