#ifndef BIGFACT_ASM_MATH_HPP_DEFINED
#define BIGFACT_ASM_MATH_HPP_DEFINED

#include <cstdint>

// Raw assembly functions
extern "C" {
    uint64_t _bigMul(uint64_t lhs, uint64_t rhs, uint64_t* over);
    uint64_t _bigDiv(uint64_t lhsHi, uint64_t lhsLo, uint64_t rhs, uint64_t* rem);
    bool     _bigAdd(uint64_t* lhs, uint64_t* rhs, std::size_t length);
    bool     _bigSub(uint64_t* lhs, uint64_t* rhs, std::size_t length);
    uint64_t _leadingBit(uint64_t op);
    uint64_t _popCount(uint64_t op);
};

// Wrappers on raw assembly functions with C++ linkage
inline uint64_t bigMul(uint64_t lhs, uint64_t rhs, uint64_t* over) {
    return _bigMul(lhs, rhs, over);
}
inline uint64_t bigDiv(uint64_t lhsHi, uint64_t lhsLo, uint64_t rhs, uint64_t* rem) {
    return _bigDiv(lhsHi, lhsLo, rhs, rem);
}
inline bool bigAdd(uint64_t* lhs, uint64_t* rhs, std::size_t length) {
    return _bigAdd(lhs, rhs, length);
}
inline bool bigSub(uint64_t* lhs, uint64_t* rhs, std::size_t length) {
    return _bigSub(lhs, rhs, length);
}
inline uint64_t leadingBit(uint64_t op) {
    return _leadingBit(op);
}
inline uint64_t popCount(uint64_t op) {
    return _popCount(op);
}

// Slow functions that should produce the same results but written in C++
namespace ref {
    uint64_t bigMul(uint64_t lhs, uint64_t rhs, uint64_t* over);
    uint64_t bigDiv(uint64_t lhsHi, uint64_t lhsLo, uint64_t rhs, uint64_t* rem);
    bool     bigAdd(uint64_t* lhs, uint64_t* rhs, std::size_t length);
    bool     bigSub(uint64_t* lhs, uint64_t* rhs, std::size_t length);
    uint64_t leadingBit(uint64_t op);
    uint64_t popCount(uint64_t op);
};

#endif
