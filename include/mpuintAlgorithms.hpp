#ifndef BIGFACT_MPUINT_ALGORITHMS_HPP_DEFINED
#define BIGFACT_MPUINT_ALGORITHMS_HPP_DEFINED

#include "mpuint.hpp"

// Only contains static members
class mpuintAlgorithms {
    public:
        mpuintAlgorithms() = delete;
        mpuintAlgorithms& operator=(mpuintAlgorithms&) = delete;

        // Multiplication

        // A standard school book algorithm. Requires very little overhead and
        // is fast for small numbers
        //
        // O(n^2)
        static mpuint schoolMul(const mpuint& lhs, const mpuint& rhs);

        // A recursive Karatsuba multiplication algorithm, also known as Toom-3
        // This is fast for larger numbers, but other algorithms are faster if
        // the numbers get too big
        //
        // O(n^(log(3) / log(2))) = O(n^1.58496....)
        static mpuint karatsubaMul(const mpuint& lhs, const mpuint& rhs);

        // Division

        // A standard school book division algorithm. Requires very little
        // overhead and is fast for small numbers
        //
        // Based on "Algorithm D" from "The Art of Computer Programming, Vol 2"
        // section 4.3.1, page 272 written by Donald Knuth.
        //
        // O(?)
        static mpuint schoolDiv(const mpuint& lhs, const mpuint& rhs, mpuint* rem);

        // Factorial

        // This is the easiest (accetable) factorial algorithm that doesn't
        //  A:  Use prime factorisations
        //  B:  Use swing factorials
        //
        // See: http://www.luschny.de/math/factorial/binarysplitfact.html
        static mpuint binSplitFact(uint64_t op);
};

#endif
