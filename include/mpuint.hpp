#ifndef BIGFACT_MPUINT_HPP_DEFINED
#define BIGFACT_MPUINT_HPP_DEFINED

#include <cstdint>
#include <ostream>
#include <initializer_list>

class mpuintAlgorithms;

class mpuint {
    private:
        // Data
        uint64_t*   data;
        std::size_t length_;

        // Signal flags
        bool borrow_;

    public:
        // Default (con|de)structors
        mpuint();
        ~mpuint();

        // Move/Copy
        mpuint(const mpuint&  val);
        mpuint(mpuint&& val);

        mpuint& operator=(const mpuint&  val);
        mpuint& operator=(mpuint&& val);

        // From uint64_t
        mpuint(uint64_t val);
        mpuint& operator=(uint64_t val);

        mpuint(const uint64_t* vals, std::size_t length);
        mpuint(std::initializer_list<uint64_t> vals);
        mpuint& operator=(std::initializer_list<uint64_t> vals);

        // Element Access (Read Only)
        uint64_t    operator[](std::size_t index) const;
        std::size_t length() const;
        bool        borrow() const;

        // Single Word Math
        mpuint& operator*=(uint64_t rhs);
        mpuint& operator/=(uint64_t rhs);

        mpuint& operator<<=(uint64_t rhs);
        mpuint& operator>>=(uint64_t rhs);

        mpuint& div(uint64_t rhs, uint64_t* rem);

        // Long Math
        mpuint& operator*=(const mpuint& rhs);
        mpuint& operator/=(const mpuint& rhs);
        mpuint& operator+=(const mpuint& rhs);
        mpuint& operator-=(const mpuint& rhs);

        mpuint& div(const mpuint& rhs, mpuint* rem);

        // Nonassigning Single Word Math
        static mpuint div(const mpuint& lhs, uint64_t rhs, uint64_t* rem);

        // Nonassigning Long Math
        static mpuint div(const mpuint& lhs, const mpuint& rhs, mpuint* rem);

    private:
        // Private modifiers
        void shrink();

        friend mpuintAlgorithms;
        friend std::ostream& operator<<(std::ostream& os, const mpuint& val);
};

// Nonassigning Single Word Math
mpuint operator*(const mpuint& lhs, uint64_t rhs);
mpuint operator/(const mpuint& lhs, uint64_t rhs);

mpuint operator<<(const mpuint& lhs, uint64_t rhs);
mpuint operator>>(const mpuint& lhs, uint64_t rhs);

uint64_t operator%(const mpuint& lhs, uint64_t rhs);

mpuint operator*(uint64_t lhs, const mpuint& rhs);

// Nonassigning Long Math
mpuint operator*(const mpuint& lhs, const mpuint& rhs);
mpuint operator/(const mpuint& lhs, const mpuint& rhs);
mpuint operator+(const mpuint& lhs, const mpuint& rhs);
mpuint operator-(const mpuint& lhs, const mpuint& rhs);

mpuint operator%(const mpuint& lhs, const mpuint& rhs);

// Comparisons
bool operator< (const mpuint& lhs, const mpuint& rhs);
bool operator<=(const mpuint& lhs, const mpuint& rhs);
bool operator==(const mpuint& lhs, const mpuint& rhs);
bool operator!=(const mpuint& lhs, const mpuint& rhs);

bool operator> (const mpuint& lhs, const mpuint& rhs);
bool operator>=(const mpuint& lhs, const mpuint& rhs);

// Print
std::ostream& operator<<(std::ostream& os, const mpuint& val);

#endif
