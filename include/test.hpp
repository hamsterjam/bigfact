#ifndef BIGFACT_TEST_HPP_DEFINED
#define BIGFACT_TEST_HPP_DEFINED

#include <iostream>

int ERR_TEST;
int ERR_TOTAL = 0;
#define startTest(msg) ERR_TEST = 0; std::cout << (msg) << std::flush
#define endTest() ERR_TOTAL += ERR_TEST; std::cout << (ERR_TEST ? "Fail" : "OK!") << std::endl
#define assert(x) ERR_TEST += (x) ? 0 : 1;
#define errors() ERR_TOTAL

#endif
