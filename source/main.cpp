#include <iostream>
#include <cstdlib>

#include "mpuint.hpp"
#include "mpuintAlgorithms.hpp"

int main(int argc, char** argv) {
    if (argc < 2) {
        std::cerr << "Requires an argument." << std::endl;
        return 1;
    }

    int op = atoi(argv[1]);
    mpuint res = std::move(mpuintAlgorithms::binSplitFact(op));

    std::cout << res << std::endl;
    return 0;
}
