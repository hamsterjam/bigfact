#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cassert>
#include <cmath>

#include <deque>
#include <ostream>
#include <initializer_list>
#include <stdexcept>

#include "mpuint.hpp"
#include "mpuintAlgorithms.hpp"
#include "asmMath.hpp"

// Default (con|de)structors
mpuint::mpuint() {
    data = (uint64_t*) malloc(sizeof(uint64_t));
    length_  = 1;

    data[0] = 0;

    borrow_  = false;
}

mpuint::~mpuint() {
    if (data) free(data);
}

// Move/Copy
mpuint::mpuint(const mpuint& val) {
    data    = (uint64_t*) malloc(sizeof(uint64_t) * val.length_);
    length_ = val.length_;
    borrow_ = val.borrow_;

    memcpy(data, val.data, sizeof(uint64_t) * val.length_);
}

mpuint::mpuint(mpuint&& val) {
    data    = val.data;
    length_ = val.length_;
    borrow_ = val.borrow_;

    val.data = NULL;
    val.length_  = 0;
}

mpuint& mpuint::operator=(const mpuint& val) {
    data    = (uint64_t*) realloc(data, sizeof(uint64_t) * val.length_);
    length_ = val.length_;
    borrow_ = val.borrow_;

    memcpy(data, val.data, sizeof(uint64_t) * val.length_);

    return *this;
}

mpuint& mpuint::operator=(mpuint&& val) {
    if (data) free(data);

    data    = val.data;
    length_ = val.length_;
    borrow_ = val.borrow_;

    val.data = NULL;
    val.length_  = 0;

    return *this;
}

// From uint64_t
mpuint::mpuint(uint64_t val): mpuint() {
    data[0] = val;
}

mpuint& mpuint::operator=(uint64_t val) {
    length_ = 1;
    data[0] = val;

    return *this;
}

mpuint::mpuint(const uint64_t* vals, std::size_t length) {
    data    = (uint64_t*) malloc(sizeof(uint64_t) * length);
    length_ = length;
    borrow_ = false;

    memcpy(data, vals, sizeof(uint64_t) * length);

    shrink();
}

mpuint::mpuint(std::initializer_list<uint64_t> vals) : mpuint(vals.begin(), vals.size())
{
    // Do nothing extra
}

mpuint& mpuint::operator=(std::initializer_list<uint64_t> vals) {
    *this = std::move(mpuint(vals));

    return *this;
}

// Element Access (Read Only)
uint64_t mpuint::operator[](std::size_t index) const {
    if (index >= length_) {
        throw std::out_of_range("mpuint::operator[]");
    }
    return data[index];
}
std::size_t mpuint::length() const {
    return length_;
}
bool mpuint::borrow() const {
    return borrow_;
}

// Single Word Math
mpuint& mpuint::operator*=(uint64_t rhs) {
    ++length_;
    data = (uint64_t*) realloc(data, sizeof(uint64_t) * length_);
    data[length_ - 1] = 0;

    auto temp = (uint64_t*) malloc(sizeof(uint64_t) * length_);
    temp[0] = 0;

    for (std::size_t i = 0; i < length_ - 1; ++i) {
        uint64_t hi, lo;
        lo = bigMul(data[i], rhs, &hi);

        data[i] = lo;
        temp[i + 1] = hi;
    }

    bigAdd(data, temp, length_);
    free(temp);

    shrink();
    return *this;
}

mpuint& mpuint::operator/=(uint64_t rhs) {
    return div(rhs, NULL);
}

mpuint& mpuint::operator<<=(uint64_t rhs) {
    uint64_t rhsWords = rhs / 64;
    uint64_t rhsBits  = rhs % 64;

    length_ += rhsWords + 1;
    data = (uint64_t*) realloc(data, sizeof(uint64_t) * length_);
    data[length_ - 1] = 0;

    // First shift full words across
    // Note that memcpy doesn't work as the regions probably overlap
    //TODO// See if strcpy will work
    //
    // This loop is set up funny because i is unsigned, if rhsWords = 0 bad
    // things would happen but are avoided here.
    for (std::size_t i = length_ - 1; i-- > rhsWords;) {
          data[i] = data[i - rhsWords];
    }
    memset(data, 0, sizeof(uint64_t) * rhsWords);

    if (rhsBits == 0) {
        length_ -= 1;
        return *this;
    }

    uint64_t mask = (1L << rhsBits) - 1;
    for (std::size_t i = length_ - 1; i-- > rhsWords;) {
        //TODO// write a rot operation in asm
        uint64_t rotated = data[i] << rhsBits | data[i] >> (64 - rhsBits);

        data[i + 1] |= rotated & mask;
        data[i] = rotated & ~mask;
    }

    shrink();
    return *this;
}

mpuint& mpuint::operator>>=(uint64_t rhs) {
    uint64_t rhsWords = rhs / 64;
    uint64_t rhsBits  = rhs % 64;

    if (rhsWords >= length_) {
        length_ = 1;
        data[0] = 0;
        return *this;
    }

    // First shift full words across
    // Note that memcpy doesn't work as the regions probably overlap
    //TODO// See if strcpy will work
    for (std::size_t i = 0; i < length_ - rhsWords; ++i) {
        data[i] = data[i + rhsWords];
    }
    length_ -= rhsWords;

    if (rhsBits == 0) return *this;

    // Now shift bits within the words
    data[0] >>= rhsBits;
    uint64_t mask = (1L << (64 - rhsBits)) - 1;
    for (std::size_t i = 1; i < length_; ++i) {
        //TODO// write a rot operation in asm
        uint64_t rotated = data[i] >> rhsBits | data[i] << (64 - rhsBits);

        data[i - 1] |= rotated & ~mask;
        data[i] = rotated & mask;
    }

    shrink();
    return *this;
}

mpuint& mpuint::div(uint64_t rhs, uint64_t* rem) {
    // We need to ensure that bigDiv always returns a 64 bit answer
    if (rhs <= data[length_ - 1]) {
        ++length_;
        data = (uint64_t *) realloc(data, sizeof(uint64_t) * length_);
        data[length_ - 1] = 0;
    }

    uint64_t prevQuot = 0;
    for (std::size_t i = length_ - 1; i > 0; --i) {
        uint64_t quot, rem;
        quot = bigDiv(data[i], data[i-1], rhs, &rem);

        data[i]     = prevQuot;
        data[i - 1] = rem;
        prevQuot    = quot;
    }
    if (rem) *rem = data[0];
    data[0] = prevQuot;

    shrink();
    return *this;
}

// Long Math
mpuint& mpuint::operator*=(const mpuint& rhs) {
    *this = std::move(*this * rhs);
    return *this;
}

mpuint& mpuint::operator/=(const mpuint& rhs) {
    return div(rhs, NULL);
}

mpuint& mpuint::operator+=(const mpuint& rhs) {
    // We want our array to be 1 bigger than either operand
    std::size_t retLen = (length_ > rhs.length_) ? length_ : rhs.length_;
    retLen += 1;

    data = (uint64_t*) realloc(data, sizeof(uint64_t) * retLen);
    memset(data + length_, 0, sizeof(uint64_t) * (retLen - length_));
    length_ = retLen;

    // Do the addition
    bool carry = bigAdd(data, rhs.data, rhs.length_);

    // Propogate the carry
    std::size_t i = rhs.length_;
    while (carry) {
        data[i] += 1;
        carry = (data[i] == 0);
        ++i;
    }

    shrink();
    return *this;
}

mpuint& mpuint::operator-=(const mpuint& rhs) {
    // We want our array to be the size of the larger of the two operands
    std::size_t retLen = (length_ > rhs.length_) ? length_ : rhs.length_;

    data = (uint64_t*) realloc(data, sizeof(uint64_t) * retLen);
    memset(data + length_, 0, sizeof(uint64_t) * (retLen - length_));
    length_ = retLen;

    // Do the subtractions
    borrow_ = bigSub(data, rhs.data, rhs.length_);

    // Propogate the borrow
    std::size_t i = rhs.length_;
    if (i != length_) while (borrow_) {
        borrow_  = (data[i] == 0);
        data[i] -= 1;
        ++i;
    }

    shrink();
    return *this;
}

mpuint& mpuint::div(const mpuint& rhs, mpuint* rem) {
    *this = std::move(mpuint::div(*this, rhs, rem));
    return *this;
}

// Nonassigning Single Word Math
mpuint mpuint::div(const mpuint& lhs, uint64_t rhs, uint64_t* rem) {
    mpuint ret(lhs);
    ret.div(rhs, rem);
    return ret;
}

// Nonassigning Long Math
mpuint mpuint::div(const mpuint& lhs, const mpuint& rhs, mpuint* rem) {
    if (rhs.length() == 1) {
        uint64_t wordRem;
        mpuint ret = std::move(mpuint::div(lhs, rhs[0], &wordRem));
        if (rem) *rem = mpuint(wordRem);
        return ret;
    }
    if (lhs.length() < rhs.length()) {
        if (rem) *rem = mpuint(lhs);
        return mpuint(0);
    }
    return mpuintAlgorithms::schoolDiv(lhs, rhs, rem);
}

// Private modifiers
void mpuint::shrink() {
    int i = length_ - 1;
    while (i && !data[i]) {
        --i;
    }
    length_ = i + 1;
}

// Nonassigning Single Word Math
mpuint operator*(const mpuint& lhs, uint64_t rhs) {
    mpuint ret(lhs);
    ret *= rhs;
    return ret;
}

mpuint operator/(const mpuint& lhs, uint64_t rhs) {
    return mpuint::div(lhs, rhs, NULL);
}

mpuint operator<<(const mpuint& lhs, uint64_t rhs) {
    mpuint ret(lhs);
    ret <<= rhs;
    return ret;
}

mpuint operator>>(const mpuint& lhs, uint64_t rhs) {
    mpuint ret(lhs);
    ret >>= rhs;
    return ret;
}

uint64_t operator%(const mpuint& lhs, uint64_t rhs) {
    uint64_t ret;
    mpuint::div(lhs, rhs, &ret);
    return ret;
}

mpuint operator*(uint64_t lhs, const mpuint& rhs) { return rhs * lhs; }

// Nonassigning Long Math
mpuint operator*(const mpuint& lhs, const mpuint& rhs) {
    const std::size_t cutoff = 500;
    if (lhs.length() < cutoff || rhs.length() < cutoff) {
        return mpuintAlgorithms::schoolMul(lhs, rhs);
    }
    else {
        return mpuintAlgorithms::karatsubaMul(lhs, rhs);
    }
}

mpuint operator/(const mpuint& lhs, const mpuint& rhs) {
    return mpuint::div(lhs, rhs, NULL);
}

mpuint operator+(const mpuint& lhs, const mpuint& rhs) {
    mpuint ret(lhs);
    ret += rhs;
    return ret;
}

mpuint operator-(const mpuint& lhs, const mpuint& rhs) {
    mpuint ret(lhs);
    ret -= rhs;
    return ret;
}

mpuint operator%(const mpuint& lhs, const mpuint& rhs) {
    mpuint ret;
    mpuint::div(lhs, rhs, &ret);
    return ret;
}

// Comparisons
bool operator<(const mpuint& lhs, const mpuint& rhs) {
    if (lhs.length() != rhs.length()) {
        return lhs.length() < rhs.length();
    }

    for (std::size_t i = lhs.length(); i-- > 0;) {
        if (lhs[i] != rhs[i]) {
            return lhs[i] < rhs[i];
        }
    }

    return false;
}
// Define everything in terms of less than. This is kind of slow but its fine
bool operator<=(const mpuint& lhs, const mpuint& rhs) { return !(lhs > rhs); }
bool operator==(const mpuint& lhs, const mpuint& rhs) { return !(lhs != rhs); }
bool operator!=(const mpuint& lhs, const mpuint& rhs) { return lhs < rhs || lhs > rhs; }
bool operator> (const mpuint& lhs, const mpuint& rhs) { return rhs < lhs; }
bool operator>=(const mpuint& lhs, const mpuint& rhs) { return !(lhs < rhs); }


// Print
mpuint bigPow(uint64_t b, uint64_t p) {
    if (p == 0) return mpuint(1);
    if (p == 1) return mpuint(b);

    uint64_t p2 = p / 2;

    return bigPow(b, p2) *= bigPow(b, p - p2);
}

void toBase10(const mpuint& num, uint64_t* store, std::size_t length) {
    if (length == 0) return;
    if (length == 1) {
        *store = num[0];
        return;
    }

    mpuint q, r;
    std::size_t split = length / 2;
    q = std::move(mpuint::div(num, bigPow(1e19, split), &r));

    toBase10(r, store, split);
    toBase10(q, store + split, length - split);
}

std::ostream& operator<<(std::ostream& os, const mpuint& val) {
    const double CONV_FACTOR = M_LN2 * 64.0 / M_LN10 / 19.0;
    std::size_t base10Length = ceil((double)val.length() * CONV_FACTOR);

    uint64_t* store = (uint64_t*) malloc(sizeof(uint64_t) * base10Length);
    memset(store, 0, sizeof(uint64_t) * base10Length);

    mpuint temp(val);
    toBase10(temp, store, base10Length);

    std::size_t i = base10Length - 1;
    while(i && !store[i]) --i;

    os << store[i];
    while (i--) {
        os.width(19);
        os.fill('0');
        os << store[i];
    }

    free(store);
    return os;
}
