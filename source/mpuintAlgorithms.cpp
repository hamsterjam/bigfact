#include <cstdlib>
#include <cstring>

#include "asmMath.hpp"
#include "mpuintAlgorithms.hpp"
#include "mpuint.hpp"

// A standard school book algorithm. Requires very little overhead and
// is fast for small numbers
//
// O(n^2)
mpuint mpuintAlgorithms::schoolMul(const mpuint& lhs, const mpuint& rhs) {
    mpuint ret(0);
    mpuint partial;

    for (std::size_t i = 0; i < rhs.length(); ++i) {
        partial   = lhs;
        partial  *= rhs[i];
        partial <<= i * 64;
        ret += partial;
    }

    return ret;
}

// A recursive Karatsuba multiplication algorithm, also known as Toom-3
// This is fast for larger numbers, but other algorithms are faster if
// the numbers get too big
//
// O(n^(log(3) / log(2))) = O(n^1.58496....)
mpuint mpuintAlgorithms::karatsubaMul(const mpuint& lhs, const mpuint& rhs) {
    // Fall back to single word multiplication
    // This will probably actually never be called...
    if (rhs.length() == 1) return lhs * rhs[0];
    if (lhs.length() == 1) return lhs[0] * rhs;

    // Otherwise split both lhs and rhs in half and recursively multiply
    std::size_t b = (lhs.length_ < rhs.length_)? lhs.length_ : rhs.length_;
    b /= 2;

    // We write lhs = x1 * b + x0
    //          rhs = y1 * b + y0
    mpuint x0(lhs.data, b);
    mpuint x1(lhs.data + b, lhs.length_ - b);
    mpuint y0(rhs.data, b);
    mpuint y1(rhs.data + b, rhs.length_ - b);

    mpuint z2(std::move(x1 * y1));
    mpuint z0(std::move(x0 * y0));

    // This calculates (x0 + x1) * (y0 * y1) - z0 - z2
    x0 += x1;
    y0 += y1;
    mpuint z1(std::move(x0 * y0));
    z1 -= z0;
    z1 -= z2;

    // Expand z0 and add the other 2 sub products
    std::size_t oldLen = z0.length_;

    z0.length_ = lhs.length_ + rhs.length_;
    z0.data = (uint64_t*) realloc(z0.data, sizeof(uint64_t) * z0.length_);
    memset(z0.data + oldLen, 0, sizeof(uint64_t) * (z0.length_ - oldLen));

    if (bigAdd(z0.data + b,     z1.data, z1.length_)) z0.data[b + z1.length_] = 1;
    if (bigAdd(z0.data + b + b, z2.data, z2.length_)) z0.data[b + z2.length_] = 1;

    z0.shrink();
    return z0;
}

// A standard school book division algorithm. Requires very little
// overhead and is fast for small numbers
//
// Based on "Algorithm D" from "The Art of Computer Programming II",
// section 4.3.1, page 272 written by Donald Knuth.
//
// O(?)
mpuint mpuintAlgorithms::schoolDiv(const mpuint& lhs, const mpuint& rhs, mpuint* rem) {
    std::size_t n, m;
    n = rhs.length_;
    m = lhs.length_ - n;

    mpuint q;
    q.length_ = m + 1;
    q.data = (uint64_t*) realloc(q.data, sizeof(uint64_t) * q.length_);
    memset(q.data, 0, sizeof(uint64_t) * q.length_);

    // D1: Normalize
    uint64_t d = 63 - leadingBit(rhs[n-1]);

    mpuint u(std::move(lhs << d));
    mpuint v(std::move(rhs << d));

    if (u.length_ < n + m + 1) {
        u.length_ = n + m + 1;
        u.data = (uint64_t*) realloc(u.data, sizeof(uint64_t) * u.length_);
        u.data[n + m] = 0;
    }

    // D2: Initialize j
    for (std::size_t j = m + 1; j-- > 0;) {
        // D3: Calculate qHat
        uint64_t qHat, rHat;
        if (u[j+n] >= v[n-1]) {
            // If we were to do the division normally, this is the case where
            // qHat = 2^64 which bigDiv can't handle so we detect it early.
            qHat = 0xFFFFFFFFFFFFFFFF;
            rHat = u[j+n-1] + v[n-1];
        }
        else {
            qHat = bigDiv(u[j+n], u[j+n-1], v[n-1], &rHat);
        }
        while (mpuint(qHat) * v[n-2] > mpuint({u[j+n-2], rHat})) {
            qHat -= 1;
            rHat += v[n-1];

            if (rHat <= v[n-1]) break;
        }

        // D4: Multiply and subtract
        u -= (qHat * v) << (j * 64);

        // D5: Test remainder
        q.data[j] = qHat;
        if (u.borrow()) {
            // D6: Add back
            q.data[j] -= 1;
            mpuint vReduced(v);
            vReduced.length_ = n;
            vReduced <<= j * 64;
            u += vReduced;
        }

        u.length_ = j + n;
        // D7: Loop on j
    }
    // D8: Unnormalize
    if (rem) {
        u >>= d;
        u.length_ = n;
        u.shrink();
        *rem = std::move(u);
    }

    q.shrink();
    return q;
}

// Takes 2 odd numbers and calculates the product of all
// odd numbers >first and <=last
mpuint oddPartProduct(uint64_t first, uint64_t last) {
    if (last - first <= 2) return mpuint(last);
    if (last - first <= 4) {
        uint64_t hi, lo;
        lo = bigMul(first + 2, last, &hi);
        return mpuint({lo, hi});
    }
    // And now on to the real function
    uint64_t mid = (first + last) / 2 | 1;

    return oddPartProduct(first, mid) * oddPartProduct(mid, last);
}

// This is the easiest (accetable) factorial algorithm that doesn't
//  A:  Use prime factorisations
//  B:  Use swing factorials
//
// See: http://www.luschny.de/math/factorial/binarysplitfact.html
mpuint mpuintAlgorithms::binSplitFact(uint64_t op) {
    mpuint p(1);
    mpuint r(1);

    for (uint64_t i = leadingBit(op); i-- > 0;) {
        uint64_t n = op >> i;
        uint64_t lower = (n >> 1) - 1 + ((n >> 1) & 1);
        uint64_t upper = n - 1 + (n & 1);

        p *= oddPartProduct(lower, upper);
        r *= p;
    }

    r <<= op - popCount(op);

    return r;
}
