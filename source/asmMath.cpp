#include <cstdint>
#include <cassert>

#include "asmMath.hpp"

uint64_t hi(uint64_t val) {
    return val >> 32;
}

uint64_t lo(uint64_t val) {
    return val & 0xFFFFFFFF;
}

uint64_t ref::bigMul(uint64_t lhs, uint64_t rhs, uint64_t* over) {
    uint64_t lhsLo, lhsHi, rhsLo, rhsHi;

    lhsLo = lo(lhs);
    lhsHi = hi(lhs);
    rhsLo = lo(rhs);
    rhsHi = hi(rhs);

    uint64_t resA, resB, resC;

    resA = lhsHi * rhsHi;
    resB = lhsHi * rhsLo + lhsLo * rhsHi;
    resC = lhsLo * rhsLo;

    // If a carry occurs
    if (resB < lhsHi * rhsLo) resA += 1L << 32;

    uint64_t resHi, resLo;

    resHi = resA + hi(resB);
    resLo = resC + (lo(resB) << 32);

    // If a carry occurs
    if (resLo < resC) ++resHi;

    if (over) *over = resHi;
    return resLo;
}

uint64_t div3by2(uint64_t lhsHi, uint64_t lhsLo, uint64_t rhs) {
    // Assume that division is normalized and that lhsHi is at most 32 bits
    assert(lhsHi <= 0xFFFFFFFF);
    assert(rhs > 0x8000000000000000);

    uint64_t u, v;
    u = (lhsHi << 32) + hi(lhsLo);
    v = hi(rhs);

    uint64_t res, testHi, testLo;
    res = u/v;
    testLo = bigMul(rhs, res, &testHi);

    // This loop should run at most twice
    int count = 0;
    while (testHi > lhsHi || (testHi == lhsHi && testLo > lhsLo)) {
        --res;
        testLo = bigMul(rhs, res, &testHi);

        assert(++count <= 2);
    }

    return res;
}

uint64_t ref::bigDiv(uint64_t lhsHi, uint64_t lhsLo, uint64_t rhs, uint64_t* rem) {
    // Assume that the division makes sense
    assert(rhs != 0);
    assert(lhsHi < rhs);

    // Normalization
    uint64_t N = 0xFFFFFFFFFFFFFFFF / rhs;

    uint64_t uHi, uLo, v;

    v    = rhs * N;
    uLo  = bigMul(lhsLo, N, &uHi);
    uHi += lhsHi * N;

    // Assert that the division is normalised
    assert(v > 0x8000000000000000);

    uint64_t resHi, resLo, uHiPart, uLoPart, resPart;
    // First division
    uHiPart = hi(uHi);
    uLoPart = (lo(uHi) << 32) + hi(uLo);
    resHi = div3by2(uHiPart, uLoPart, v);

    // Subtract off, the upper 32 bits should always go to 0 from this step.
    // Subtraction here may borrow... which is OK?
    resPart = uLoPart - resHi * v;
    uHi = hi(resPart);
    uLo = (lo(resPart) << 32) + lo(uLo);

    // Second division
    uHiPart = uHi;
    uLoPart = uLo;
    resLo = div3by2(uHiPart, uLoPart, v);

    // Subtract off, again upper 32 bits should go to 0 (that is uHi goes to 0)
    resPart = uLoPart - resLo * v;
    uHi = 0;
    uLo = resPart;

    // At this stage uLo is the remainder
    if (rem) *rem = uLo / N;

    // Combine our resLo and resHi (which should be 32 bit)
    return (resHi << 32) + resLo;
}

bool ref::bigAdd(uint64_t* lhs, uint64_t* rhs, std::size_t length) {
    bool carry = false;

    for (std::size_t i = 0; i < length; ++i) {
        lhs[i] += rhs[i];
        if (carry) ++lhs[i];
        if (lhs[i] < rhs[i]) carry = true;
        else                 carry = false;
    }

    return carry;
}

bool ref::bigSub(uint64_t* lhs, uint64_t* rhs, std::size_t length) {
    bool borrow = false;

    for (std::size_t i = 0; i < length; ++i) {
        uint64_t old = lhs[i];
        lhs[i] -= rhs[i];
        if (borrow) --lhs[i];
        if (lhs[i] > old) borrow = true;
        else              borrow = false;
    }

    return borrow;
}

uint64_t ref::leadingBit(uint64_t op) {
    uint64_t ret = 0;
    while (!(op & 0x8000000000000000)) {
        ret += 1;
        op <<= 1;
    }

    return 63 - ret;
}

uint64_t ref::popCount(uint64_t op) {
    // This is deliberately naiive
    uint64_t ret = 0;
    for (int i = 0; i < 64; ++i) {
        ret += op & 1;
        op >>= 1;
    }

    return ret;
}
