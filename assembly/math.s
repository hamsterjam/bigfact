# Just a reminder to anyone reading this.
# 64 bit C passes arguments mostly via registers in the following order:
#
#   rdi, rsi, rdx, rcx, r8, r9
#
# It then expects the return value to be passed in rax

.global _bigMul
.global _bigDiv
.global _bigAdd
.global _bigSub
.global _leadingBit
.global _popCount

# uint64_t _bigMul(uint64_t lhs, uint64_t rhs, uint64_t* over)
#
# Calculates lhs * rhs and returns the result. If the operation would overflow
# the upper word of the result is returned in over
_bigMul:
    movq    %rdx,   %r8     # mul will clober rdx, move to a different register
    movq    %rdi,   %rax    # mul takes an implicit argument in rax
    mulq    %rsi
    cmpq    $0,     %r8     # If over is NULL, don't write anything to it
    je      1f
    movq    %rdx,   (%r8)   # move the overflow to the third argument
1:  ret

# uint64_t _bigDiv(uint64_t lhsHi, uint64_t lhsLo, uint64_t rhs, uint64_t* rem)
#
# Calculates lhsHi:lhsLo / rhs and returns the quotient. The remainder is
# returned in rem. This should only be called when you can be sure the result
# makes sense (i.e. no division by zero) and when the result is guaranteed to
# be at most a 64 bit number
_bigDiv:
    movq    %rdx,   %r8     # div clobers rdx, move to a different register
    movq    %rdi,   %rdx    # div takes implicit arguments in rdx:rax
    movq    %rsi,   %rax
    divq    %r8
    cmpq    $0,     %rcx    # If rem is NULL, don't write anything to it
    je      1f
    movq    %rdx,   (%rcx)  # move the remainder to the fourth argument
1:  ret

# bool _bigAdd(uint64_t* lhs, uint64_t* rhs, size_t length)
#
# Treats lhs and rhs as arrays and adds values in rhs to values in lhs storing
# the value in lhs. If an addition overflows, it will carry to the next cell.
# The value of the carry flag will be returned
_bigAdd:
    movq    %rdx,   %rcx    # loop uses the rcx register to track the loop
    xorq    %rdx,   %rdx    # Zero out the rdx register for the offset
    xorq    %rax,   %rax    # Zero out the return value
    clc                     # Clear the carry flag

1:  movq    (%rsi, %rdx, 8), %r8 # We can't offset 2 memory values at once
    adcq    %r8, (%rdi, %rdx, 8) #   so we store one in a register first
    incq    %rdx
    loop    1b              # Decrement ecs and loop if ecx != 0

    adcq    $0,     %rax    # Add a zero to get the value of the carry flag
    ret

# bool _bigSub(uint64_t* lhs, uint64_t* rhs, size_t length)
#
# Treats lhs and rhs as arrays and subtractes value in rhs from value in lhs
# storing the result in lhs. If a subtraction borrows, it will borrow from the
# next cell. The value of the carry flag (borrow) is returned
_bigSub:
    movq    %rdx,   %rcx    # loop uses the rcx register to track the loop
    xorq    %rdx,   %rdx    # Zero out the rdx register for the offset
    xorq    %rax,   %rax    # Zero out the return value
    clc                     # Clear the carry flag

1:  movq    (%rsi, %rdx, 8), %r8 # We can't offset 2 memory values at once
    sbbq    %r8, (%rdi, %rdx, 8) #   so we store one in a register first
    incq    %rdx
    loop    1b              # Decrement ecx and loop if ecx != 0

    adcq    $0,     %rax    # Add a zero to get the value of the carry flag
    ret

# uint64_t _leadingBit(uint64_t op)
#
# Returns the bit position of the most significant set bit of op
_leadingBit:
    bsrq    %rdi,   %rax
    ret

# uint64_t _popCount(uint64_t op)
#
# Returns the number of set bits in op. Also known as a sideways add.
_popCount:
    popcntq %rdi,   %rax
    ret
