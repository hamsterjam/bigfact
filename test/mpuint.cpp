#include <cstdint>
#include <cstring>

#include <random>
#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>

#include "mpuint.hpp"
#include "test.hpp"

uint64_t rand64(uint64_t min = 0, uint64_t max = 0xFFFFFFFFFFFFFFFF) {
    static std::random_device rd;
    static std::default_random_engine gen(rd());
    std::uniform_int_distribution<uint64_t> dist(min, max);

    return dist(gen);
}

int main(int argc, char** argv) {
    { // creation and destruction
        startTest("Testing creation and destruction...\t\t");

        // Default
        {
            mpuint num;
            assert(num.length() == 1);
            assert(num[0] == 0);
        }
        for (int i = 0; i < 1e3; ++i) {
            // From uint64_t
            {
                uint64_t val = rand64();
                mpuint num(val);

                assert(num.length() == 1);
                assert(num[0] == val);
            }
            // Initializer list
            {
                uint64_t val1 = rand64();
                uint64_t val2 = rand64();
                uint64_t val3 = rand64(1);

                mpuint num({val1, val2, val3});

                assert(num.length() == 3);
                assert(num[0] == val1);
                assert(num[1] == val2);
                assert(num[2] == val3);
            }
            // From array / Copy / Move
            {
                uint64_t vals[100];
                for (int j = 0; j < 99; ++j) {
                    vals[j] = rand64();
                }
                vals[99] = rand64(1);

                mpuint num1(vals, 100);
                mpuint num2(num1);

                assert(num1.length() == 100);
                assert(num2.length() == 100);

                for (int j = 0; j < 100; ++j) {
                    assert(num1[j] == vals[j]);
                    assert(num2[j] == vals[j]);
                }

                mpuint num3(std::move(num1));

                for (int j = 0; j < 100; ++j) {
                    assert(num3[j] == vals[j]);
                }
            }
            // Shrinking of from array
            {
                uint64_t vals[100];
                memset(vals, 0, sizeof(uint64_t) * 100);
                vals[0] = rand64();

                mpuint num(vals, 100);

                assert(num.length() == 1);
                assert(num[0] == vals[0]);
            }
        }

        endTest();
    }
    { // assignment operators
        startTest("Testing assignment operators...\t\t\t");

        for (int i = 0; i < 1e3; ++i) {
            // From uint64_t
            {
                mpuint num;

                uint64_t val = rand64();
                num = val;

                assert(num.length() == 1);
                assert(num[0] == val);

            }
            // From init list
            {
                mpuint num;

                uint64_t val1 = rand64();
                uint64_t val2 = rand64();
                uint64_t val3 = rand64(1);

                num = {val1, val2, val3};

                assert(num.length() == 3);
                assert(num[0] == val1);
                assert(num[1] == val2);
                assert(num[2] == val3);
            }
            // Copy / Move
            {
                mpuint num1;

                uint64_t vals[100];
                for (int j = 0; j < 99; ++j) {
                    vals[j] = rand64();
                }
                vals[99] = rand64(1);
                mpuint num2(vals, 100);

                num1 = num2;
                assert(num1.length() == 100);
                for (int j = 0; j < 100; ++j) {
                    assert(num1[j] == vals[j]);
                }

                mpuint num3;

                num3 = std::move(num2);
                assert(num3.length() == 100);
                for (int j = 0; j < 100; ++j) {
                    assert(num3[j] == vals[j]);
                }
            }
        }

        endTest();
    }
    { // Out of range indexing
        startTest("Testing out of range indexing...\t\t");

        mpuint num;

        bool caught = false;
        try {
            num[1];
        }
        catch (std::out_of_range& e) {
            caught = true;
        }

        assert(caught);

        endTest();
    }
    { // uint64_t add/sub inverse
        startTest("Testing uint64_t add/sub inverse...\t\t");

        for (int i = 0; i < 1e3; ++i) {
            // Generate a mpuint 100 words long
            uint64_t data[100];
            for (int j = 0; j < 100; ++j) {
                data[j] = rand64();
            }
            uint64_t op = rand64();
            mpuint start(data, 100);
            mpuint finish = start + op - op;

            assert(start == finish);
        }

        endTest();
    }
    { // uint64_t add carry propogation
        startTest("Testing uint64_t add carry propogation...\t");

        uint64_t data[100];
        memset(data, 0xFF, sizeof(uint64_t) * 100);

        mpuint num(data, 100);
        assert(num + 1 == mpuint(1) << 64 * 100);

        endTest();
    }
    { // uint64_t sub borrow propogation
        startTest("Testing uint64_t sub borrow propogation...\t");

        uint64_t dataA[100];
        memset(dataA, 0, sizeof(uint64_t) * 100);
        dataA[99] = 1;

        uint64_t dataB[100 - 1];
        memset(dataB, 0xFF, sizeof(uint64_t) * (100 - 1));

        mpuint numA(dataA, 100);
        mpuint numB(dataB, 100 - 1);
        assert(numA - 1 == numB);

        endTest();
    }
    { // uint64_t mul/div inverse
        startTest("Testing uint64_t mul/div inverse...\t\t");

        for (int i = 0; i < 1e3; ++i) {
            // Generate a mpuint 100 words long
            uint64_t data[100];
            for (int j = 0; j < 100; ++j) {
                data[j] = rand64();
            }
            uint64_t op1 = rand64(1);
            uint64_t op2 = rand64(0, op1);
            mpuint start(data, 100);

            // This tests reversed * and + operators as well as the % operator
            mpuint partial = op2 + op1 * start;
            assert(start == partial / op1);
            assert(partial % op1 == op2);

            // Test assignment operators as well
            mpuint res(start);
            res *= op1;
            res += op2;
            res /= op1;
            assert(res == start);
        }

        endTest();
    }
    { // uint64_t mul/div transitivity
        startTest("Testing uint64_t mul/div transitivity...\t");

        for (int i = 0; i < 1e3; ++i) {
            uint64_t data[100];
            for (int j = 0; j < 100; ++j) {
                data[j] = rand64();
            }
            // This generates a random even number
            uint64_t op = rand64(0, 0x7FFFFFFFFFFFFFFF) << 1;
            mpuint num(data, 100);

            assert((num * 2) / op == num / (op / 2));
        }
        endTest();
    }
    { // uint64_t shift inverse
        startTest("Testing uint64_t shift inverse...\t\t");

        for (int i = 0; i < 1e3; ++i) {
            // Generate a mpuint 100 words long
            uint64_t data[100];
            for (int j = 0; j < 100; ++j) {
                data[j] = rand64();
            }
            uint64_t op = rand64(0, 6400);
            mpuint start(data, 100);
            mpuint finish = start << op >> op;

            assert(start == finish);
        }

        endTest();
    }
    { // uint64_t shift to zero
        startTest("Testing uint64_t shift to zero...\t\t");

        uint64_t data[100];
        memset(data, 0xFF, sizeof(uint64_t) * 100);
        mpuint num(data, 100);

        num >>= 6400;

        assert(num.length() == 1);
        assert(num[0] == 0);

        endTest();
    }
    { // upper word length comparisons
        startTest("Testing upper word length comparisons...\t");

        for (int i = 0; i < 1e3; ++i) {
            uint64_t rawA = rand64();
            uint64_t rawB = rand64();

            mpuint numA(rawA);
            mpuint numB(rawB);

            assert((numA < numB) == (rawA < rawB));
        }

        endTest();
    }
    { // differing length comparisons
        startTest("Testing differing length comparisons...\t\t");

        uint64_t data[100];
        memset(data, 0xFF, sizeof(uint64_t) * 100);

        for (int i = 0; i < 1e3; ++i) {
            std::size_t lengthA = rand64(1, 100);
            std::size_t lengthB = rand64(1, 100);

            mpuint numA(data, lengthA);
            mpuint numB(data, lengthB);

            assert((numA < numB) == (lengthA < lengthB));
        }

        endTest();
    }
    { // off-by-one length comparisons
        startTest("Testing off-by-one length comparisons...\t");

        for (int i = 0; i < 1e3; ++i) {
            // Generate a mpuint 100 words long
            uint64_t data[100];
            for (int j = 0; j < 100; ++j) {
                data[j] = rand64();
            }
            mpuint start(data, 100);
            start += 1; // Dissallow 0;

            // Doing things off by 1 makes this the worst case scenario
            assert(start == start);
            assert(start != start + 1);
            assert(start < start + 1);
            assert(start <= start);
            assert(start <= start + 1);
            assert(start > start - 1);
            assert(start >= start);
            assert(start >= start - 1);
        }

        endTest();
    }
    { // mpuint add/sub inverse
        startTest("Testing mpuint add/sub inverse...\t\t");

        for (int i = 0; i < 1e3; ++i) {
            uint64_t dataA[100];
            uint64_t dataB[100];
            for (int j = 0; j < 100; ++j) {
                dataA[j] = rand64();
                dataB[j] = rand64();
            }
            mpuint numA(dataA, 100);
            mpuint numB(dataB, 100);

            // Doing addition first makes no assumption about sign
            assert(numA == numA + numB - numB);

            // Doing subtraction first requires some more careful thought.
            // This is sort of out of scope for an unsigned type though...
            mpuint resA = std::move(numA - numB);
            bool under = resA.borrow();
            mpuint resB = std::move(resA + numB);
            if (under) resB -= mpuint(1) << 64 * 100;

            assert(numA == resB);
        }

        endTest();
    }
    { // mpuint mul/div inverse
        startTest("Testing mpuint mul/div inverse...\t\t");

        for (int i = 0; i < 1e3; ++i) {
            std::size_t len = 100;

            uint64_t dataA[len];
            uint64_t dataB[len];
            for (unsigned int j = 0; j < len; ++j) {
                dataA[j] = rand64();
                dataB[j] = rand64();
            }
            mpuint numA(dataA, len);
            mpuint numB(dataB, len);
            mpuint numC = numB - rand64(1);

            mpuint partial = (numA * numB) + numC;

            assert(numA == partial / numB);
            assert(numC == partial % numB);

            // Test assignment operators as well
            mpuint res(numA);
            res *= numB;
            res /= numB;
            assert(numA == res);
        }

        endTest();
    }
    {
        startTest("Testing mpuint div contrived cases...\t\t");

        const uint64_t max = 0xFFFFFFFFFFFFFFFF;
        const uint64_t top = 0x8000000000000000;

        {
            // Triggers the case where the upper word of u is equal to the
            // upper word of v
            mpuint divisor({max, top});
            mpuint dividend = divisor * max - 1;
            assert(dividend / divisor == max - 1);
        }
        {
            // Triggers sub-algorithm D6
            mpuint divisor({max, 0, top});
            mpuint dividend = mpuint({0, top}) * max << 64;
            assert(dividend / divisor == max - 1);
        }
        {
            // Test dividing by a single word
            mpuint divisor(rand64(1));
            mpuint dividend({rand64(), rand64()});
            assert(dividend / divisor == dividend / divisor[0]);
        }
        {
            // Test the case where the divisior is longer than the dividend
            mpuint divisor({rand64(), rand64(1)});
            mpuint dividend(rand64());
            mpuint q, r;
            q = mpuint::div(dividend, divisor, &r);
            assert(dividend / divisor == 0);
            assert(r == dividend);
        }

        endTest();
    }
    { // print function
        startTest("Testing print function...\t\t\t");

        for (int i = 0; i < 1e3; ++i) {
            std::stringstream ref;
            std::stringstream test;

            mpuint num;
            for (int j = 0; j < 100; ++j) {
                uint64_t val = rand64(0, 1e19);
                num *= 1e19;
                num += val;

                ref << val;

                ref.width(19);
                ref.fill('0');
            }

            test << num;
            assert(ref.str() == test.str());
        }

        endTest();
    }

    return 0;
}
