#include <cstdint>
#include <random>

#include <iostream>

#include "test.hpp"
#include "asmMath.hpp"

uint64_t rand64(uint64_t min = 0, uint64_t max = 0xFFFFFFFFFFFFFFFF) {
    static std::random_device rd;
    static std::default_random_engine gen(rd());
    std::uniform_int_distribution<uint64_t> dist(min, max);

    return dist(gen);
}

int main(int argc, char** argv) {
    {
        startTest("Testing NULL over/rem...\t");

        uint64_t resD = bigDiv(1, 2, 2, NULL);
        uint64_t resM = bigMul(resD, 2, NULL);
        assert(resM == 2);

        endTest();
    }
    {
        startTest("Testing bigMul...\t\t");
        for (int i = 0; i < 1e6; ++i) {
            uint64_t lhs, rhs, refHi, refLo, testHi, testLo;

            lhs = rand64();
            rhs = rand64();

            refLo  = ref::bigMul(lhs, rhs, &refHi);
            testLo =      bigMul(lhs, rhs, &testHi);

            assert(refHi == testHi);
            assert(refLo == testLo);
        }

        endTest();
    }
    {
        startTest("Testing bigDiv...\t\t");
        for (int i = 0; i < 1e6; ++i) {
            uint64_t lhsHi, lhsLo, rhs, refQ, refR, testQ, testR;

            rhs   = rand64(1);
            lhsLo = rand64();
            lhsHi = rand64(0, rhs - 1);

            refQ  = ref::bigDiv(lhsHi, lhsLo, rhs, &refR);
            testQ =      bigDiv(lhsHi, lhsLo, rhs, &testR);

            assert(refQ == testQ);
            assert(refR == testR);
        }

        endTest();
    }
    {
        startTest("Testing bigAdd...\t\t");

        for (int i = 0; i < 1e4; ++i) {
            uint64_t refLhs[100];
            uint64_t testLhs[100];
            uint64_t rhs[100];

            for (int j = 0; j < 100; ++j) {
                refLhs[j]  = rand64();
                testLhs[j] = refLhs[j];
                rhs[j]     = rand64();;
            }

            bool refC, testC;
            refC  = ref::bigAdd(refLhs,  rhs, 100);
            testC =      bigAdd(testLhs, rhs, 100);

            assert(refC == testC);

            bool eq = true;
            for (int j = 0; j < 100; ++j) {
                if (refLhs[j] != testLhs[j]) {
                    eq = false;
                    break;
                }
            }
            assert(eq);
        }

        endTest();
    }
    {
        startTest("Testing bigSub...\t\t");

        for (int i = 0; i < 1e4; ++i) {
            uint64_t refLhs[100];
            uint64_t testLhs[100];
            uint64_t rhs[100];

            for (int j = 0; j < 100; ++j) {
                refLhs[j]  = rand64();
                testLhs[j] = refLhs[j];
                rhs[j]     = rand64();;
            }

            bool refB, testB;
            refB  = ref::bigSub(refLhs,  rhs, 100);
            testB =      bigSub(testLhs, rhs, 100);

            assert(refB == testB);

            bool eq = true;
            for (int j = 0; j < 100; ++j) {
                if (refLhs[j] != testLhs[j]) {
                    eq = false;
                    break;
                }
            }
            assert(eq);
        }

        endTest();
    }
    {
        startTest("Testing leadingBit...\t\t");

        for (int i = 0; i < 1e6; ++i) {
            uint64_t op = rand64();

            assert(ref::leadingBit(op) == leadingBit(op));
        }

        endTest();
    }
    {
        startTest("Testing popCount...\t\t");

        for (int i = 0; i < 1e6; ++i) {
            uint64_t op = rand64();

            assert(ref::popCount(op) == popCount(op));
        }

        endTest();
    }
    return 0;
}
